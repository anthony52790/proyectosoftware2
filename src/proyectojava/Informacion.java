package proyectojava;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
public class Informacion {
    
    String nombre;
    String contrasena;
    String texto;
    
    private File mensaje = null;
    private FileWriter escribir ;
    private FileReader leer;
    private BufferedReader contenido;
    
    public Informacion(String Nombre,String Contrasena){
        this.nombre=Nombre;
        this.contrasena=Contrasena;
        texto="";
    }
    public void datoUsuario(){
       try{
           mensaje= new File("dato.txt");
           escribir = new FileWriter(mensaje,true);              
           escribir.write(nombre+";"+contrasena+"\r\n");   

           JOptionPane.showMessageDialog(null,"Informacion Guardada\ncon exito");
           escribir.close();
       }catch(IOException ex){
           System.out.println(ex.getMessage());
       }
    }
    
   public int datoBuscar(String nombre,String contraseña){
        try {
            leer = new FileReader("dato.txt");
            contenido = new BufferedReader(leer);            

            while((texto=contenido.readLine())!=null){
                 String[] arr = texto.split(";");
                 if(nombre.compareTo(arr[0])==0 && contraseña.compareTo(arr[1])==0){
                     return 1;
                 }
                 
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return 2;
        }
        return 0;
   }
}
